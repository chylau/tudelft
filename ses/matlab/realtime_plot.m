

%% Clear all
clc
clear

%% Define plot window and size
set(gcf, 'Position', [300, 200 1400, 600])

%% Set-up Arduino communication
delete(instrfindall);
s=serial('COM4', 'BaudRate',9600);
fopen(s);


%% Define origin and LDR positions
origin=[0,0,0];
tri=[-1 -1 2 -1; -1 2 -1 -1];
truevec=[0 0 1]

%% Select 2D(0) or 3D(1) plot
type=1;


%% 2D
if type==0
    while (1)
        dat=fscanf(s, '%f'); 
        %Reset x,y,z for next sensor value
        x=0;
        y=0;
        z=0;
        
        if dat==999 %% Identify header
            %%Get x and y value
            x = fscanf(s,'%f');  
            y = fscanf(s,'%f');

        end
        %%%Plot vector arrow in 2D
        quiver(0,0,x,y)
        grid on
        axis([-150 150 -150 150])
        drawnow
    end 
   
end

%% 3D
if type ==1
    while (1)
        
        dat=fscanf(s, '%f');
        %Reset x,y,z for next sensor value
        x=0;
        y=0;
        z=0;
        if dat==999 %% Identify header
            %%Get x, y and z value
     
            x = fscanf(s,'%f');
            y = fscanf(s,'%f');
            z = fscanf(s,'%f');
        end
        %% Define vector
        vector=[x,y,z];
        %% Calculate accuracy angle
        CosTheta = dot(truevec,vector)/(norm(truevec)*norm(vector));
        ThetaInDegrees = acosd(CosTheta)
        
        
        subplot(1,2,1)
            %% 3D plot
         v=[origin;vector];
         plot([0 0 0],'ro')
         axis([-150 150 -150 150 -150 150])
         hold on; 
         plot3(v(:,1),v(:,2),v(:,3),'r')
         %mArrow3(origin,vector)
         %view([-45 35]);
         grid on;
         
         
         
         
         subplot(1,2,2)
         %% 2D plot
         plot(tri(1,:), tri(2,:), 'linewidth',1)
         hold on;
         quiver(0,0,x,y)
         grid on
         axis([-5 5 -5 5 -5 5])
         drawnow
         clf
         
         
    end
end
    
    
%% Close serial port
fclose(s);
delete(s);