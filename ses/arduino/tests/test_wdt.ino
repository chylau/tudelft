#include <avr/wdt.h>

const int button = 2;     

void setup() {
  wdt_enable(WDTO_4S);
  Serial.begin(9600);
  
  pinMode(button, INPUT);

  /* Interrupt vector: 0 corresponds to digital pin 2 */
  attachInterrupt(0, pin_ISR, CHANGE);
}

void loop() {
  Serial.println("STATUS: OK.");
  wdt_reset();
}

void pin_ISR() {
  /* Enter infinite loop */
  while(1);
}
