/* Number of sensors */
const int NUM_SENSORS = 3;

/* Sensor calibration array */
int calibration_sensors[NUM_SENSORS];

/* Sensor array: current */
int cur_sensors[NUM_SENSORS];

/* Sensor array: previous */
int prev_sensors[NUM_SENSORS];

/* Multiplexer select pins */
int s0 = 5;
int s1 = 6;
int s2 = 7;

/* Multiplexer select values */
int r0 = 0;
int r1 = 0;
int r2 = 0;

void calibrate_sensors(void) {
  for(int i = 0; i < NUM_SENSORS; i++) {
    r0 = bitRead(i, 0); 
    r1 = bitRead(i, 1); 
    r2 = bitRead(i, 2); 

    digitalWrite(s0, r0);
    digitalWrite(s1, r1);
    digitalWrite(s2, r2);
    
    calibration_sensors[i] = analogRead(A0);
  }
}

void setup() {
  Serial.begin(9600);

  /* Set select pins of the multiplexer as output type */
  pinMode(s0, OUTPUT);
  pinMode(s1, OUTPUT);
  pinMode(s2, OUTPUT);

  /* Set initial sensor values for calibration */
  calibrate_sensors();
}

void loop() {
  /* Transmit header */
  //Serial.println(999);
  
  /* Set current sensor array */
  for(int i = 0; i < NUM_SENSORS; i++) {
    r0 = bitRead(i, 0);
    r1 = bitRead(i, 1); 
    r2 = bitRead(i, 2); 

    digitalWrite(s0, r0);
    digitalWrite(s1, r1);
    digitalWrite(s2, r2);
    
    cur_sensors[i] = analogRead(A0) - calibration_sensors[i];
    //Serial.println(cur_sensors[i]);
  }

  /* Copy curent array contens to previous array */
  for(int i = 0; i < NUM_SENSORS; i++) {
    prev_sensors[i] = cur_sensors[i];
  }

  Serial.println(cur_sensors[0]);
  
  delay(1000);
}
