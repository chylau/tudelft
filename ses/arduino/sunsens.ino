/* 
 *  Sun sensing program: computes and transmits the cartesian coordinates of the light source 
 *  Model: Arduino UNO
 *  University: TU Delft, The Netherlands
 *  Year: 2017-2018
 *  Course: Space Embedded Systems, AE4265
 *  Supervisor: A. Menicucci
 *  Authors: Sonny Lie (4351177) and Chy Lau (4155955)
 */

#include <avr/wdt.h>

/* The following variables marked in the comments with [USER CAN EDIT] must be edited to fit the setup */

/* [USER CAN EDIT] enable = 0 for 2D computation, enable = 1 for 3D computation */
int enable = 1;

/* [USER CAN EDIT] Number of sensors */
const int NUM_SENSORS = 3;

/* [USER CAN EDIT] X, Y and Z direction array */
double xdir[] = {-1.0, 2.0, -1.0};
double ydir[] = {-1.0, -1.0, 2.0};
double zdir[] = {1.0, 1.0, 1.0};

/* [USER CAN EDIT] LDR redundancy: redundancy = 0 to disable, redundancy = 1 to enable */
int redundancy = 0;

/* [USER CAN EDIT] Noise correction: noise = 0 to disable, noise = 1 to enable */
int noise = 0;

/* [USER CAN EDIT] Thresholds: treshold1 - maximum difference between current and previous sensor value, treshold2 - number of counts (20 corresponds to 20*delay = 20*100ms = 2 seconds) */
int threshold1 = 500;
int threshold2 = 20;

/* Sensor calibration array */
int calibration_sensors[NUM_SENSORS] = {0};

/* Sensor array: current */
double cur_sensors[NUM_SENSORS] = {0};

/* Sensor array: previous */
double prev_sensors[NUM_SENSORS] = {0};

/* Sensor mean value array */
double mean_sensors[NUM_SENSORS/2] = {0};

/* Multiplexer select pins */
int s0 = 5;
int s1 = 6;
int s2 = 7;

/* Multiplexer select values */
int r0 = 0;
int r1 = 0;
int r2 = 0;

/* Counter */
int count[NUM_SENSORS] = {0};

/* Check array: keeps track of the faulty sensors */
int check[NUM_SENSORS] = {0};

/* X, Y and Z coordinates */
double x = 0;
double y = 0;
double z = 0;

void calibrate_sensors(void) {
  for(int i = 0; i < NUM_SENSORS; i++) {
    r0 = bitRead(i, 0); 
    r1 = bitRead(i, 1); 
    r2 = bitRead(i, 2); 

    digitalWrite(s0, r0);
    digitalWrite(s1, r1);
    digitalWrite(s2, r2);
    
    calibration_sensors[i] = analogRead(A0);
  }
}

void setup() {
  wdt_enable(WDTO_4S);
  Serial.begin(9600);

  /* Set select pins of the multiplexer as output type */
  pinMode(s0, OUTPUT);
  pinMode(s1, OUTPUT);
  pinMode(s2, OUTPUT);

  /* Set initial sensor values for calibration */
  calibrate_sensors();
}

void loop() {
  /* Transmit header */
  Serial.println(999);

  /* Reset X, Y and Z coordinates */
  x = 0;
  y = 0;
  z = 0;
  
  /* Set current sensor array */
  for(int i = 0; i < NUM_SENSORS; i++) {
    r0 = bitRead(i, 0);
    r1 = bitRead(i, 1); 
    r2 = bitRead(i, 2); 

    digitalWrite(s0, r0);
    digitalWrite(s1, r1);
    digitalWrite(s2, r2);
    
    cur_sensors[i] = analogRead(A0) - calibration_sensors[i];
  }

  /* LDR2: a random value induced by noise: detect if the difference between the current and previous is larger than the threshold and correct */
  if(noise) {
    if(prev_sensors != 0) {
      for(int i = 0; i < NUM_SENSORS; i++) {
        int delta = 0;
        delta = abs(cur_sensors[i] - prev_sensors[i]);
        if(delta > threshold1) {
          cur_sensors[i] = prev_sensors[i];
        }
      }
    }
  }

  /* LDR1: LDR outputs constant value: detect if value is stuck for 2 seconds and correct */
  if(redundancy) {
    if(prev_sensors != 0) {
      for(int i = 0; i < NUM_SENSORS/2; i++) {
        int delta1 = 0;
        int delta2 = 0;
        delta1 = cur_sensors[i*2] - prev_sensors[i*2];
        delta2 = cur_sensors[i*2+1] - prev_sensors[i*2+1];
       
        if(delta1 == 0) {
          count[i] += 1;
        } else {
          if(!check[i*2]) {
            count[i] = 0;
          }
        }

        if(delta2 == 0) {
          count[i*2+1] += 1;
        } else {
          if(!check[i*2+1]) {
            count[i*2+1] = 0;
          }
        }
  
        if(count[i*2] > threshold2) {
          cur_sensors[i*2] = cur_sensors[i*2+1];
          check[i*2] = 1;
        }

        if(count[i*2+1] > threshold2) {
          cur_sensors[i*2+1] = cur_sensors[i*2];
          check[i*2+1] = 1;
        }
      }
    }
  }

  /* Copy curent array contens to previous array */
  for(int i = 0; i < NUM_SENSORS; i++) {
    prev_sensors[i] = cur_sensors[i];
  }

  /* Calculate mean value of each vertex */
  if(redundancy) {
    for(int i = 0; i < NUM_SENSORS/2; i++) {
      mean_sensors[i] = (cur_sensors[i*2] + cur_sensors[i*2+1])/2;
    }
  }
  
  /* Calculate 2D vector X- and Y-coordinates */
  if(!enable) {
    if(redundancy) {
      for(int i = 0; i < NUM_SENSORS/2; i++) {
        x += xdir[i] * mean_sensors[i];
        y += ydir[i] * mean_sensors[i];
      }
    } else {
      for(int i = 0; i < NUM_SENSORS; i++) {
        x += xdir[i] * cur_sensors[i];
        y += ydir[i] * cur_sensors[i];
      }
    }
      Serial.println(x,4);
      Serial.println(y,4);
  }

  /* Calculate 3D vector X- and Y-coordinates */
  if(enable) {
    if(redundancy) {
      for(int i = 0; i < NUM_SENSORS/2; i++) {
        x += xdir[i] * mean_sensors[i];
        y += ydir[i] * mean_sensors[i];
        z += zdir[i] * mean_sensors[i];
      }
    } else {
      for(int i = 0; i < NUM_SENSORS; i++) {
        x += xdir[i] * cur_sensors[i];
        y += ydir[i] * cur_sensors[i];
        z += zdir[i] * cur_sensors[i];
      }
    }

    Serial.println(x);
    Serial.println(y);
    Serial.println(z);
  }
  
  delay(100);
  wdt_reset();
}
