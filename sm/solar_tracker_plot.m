%% -- Real-time solar tracking error response curves
    % Model: Arduino UNO
    % University: TU Delft, The Netherlands
    % Year: 2018-2019
    % Course: Space Mechatronics, AE4S06
    % Supervisor: C.J.M. Verhoeven and M.K. Verma
    % Author: Chy Lau (4155955)
%%

clc;
clear;

delete(instrfindall);
s=serial('/dev/ttyUSB0', 'BaudRate',9600);
fopen(s);

setpoint = 0;

figure(1);
grid on;
h1 = line(0,0);
xlabel ('Time [s]'), ylabel('Error');

k = 1; % index
d1 = 0; % error 1
d2 = 0; % error 2
t = 0; % time

tic;

while ishandle(h1)
    k = k + 1;
    t(k) = toc;
    dat = fscanf(s, '%f');
    if dat == 999
        d1(k) = fscanf(s, '%f');
        d2(k) = fscanf(s, '%f');
    else
        d1(k) = 0;
        d2(k) = 0;
    end
    
    ref = refline([0 setpoint]);
    ref.Color = 'r';
    h1 = line([t(k-1), t(k)], [d1(k-1), d1(k)], 'Color', 'b');
    h1 = line([t(k-1), t(k)], [d2(k-1), d2(k)], 'Color', 'm');
    axis([0, t(k)+10, -100, 200]);
    
    
    drawnow;
end

fclose(s); % close serial port
delete(s); % remove serial port from memory