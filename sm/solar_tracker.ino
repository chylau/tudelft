/* 
 *  Solar tracking program: tracks a light source using LDRs and servomotors
 *  Model: Arduino UNO
 *  University: TU Delft, The Netherlands
 *  Year: 2018-2019
 *  Course: Space Mechatronics, AE4S06
 *  Supervisor: C.J.M Verhoeven and M.K. Verma
 *  Author: Chy Lau (4155955)
 */

#include <Servo.h>

/* The following variables marked in the comments with [USER CAN EDIT] must be edited to fit the setup */

/* [USER CAN EDIT] Number of sensors */
const int NUM_SENSORS = 8;

/* [USER CAN EDIT] Threshold1: current and previous value */
const int threshold1 = 500;

/* [USER CAN EDIT] Threshold2: frozen time */
const int threshold2 = 20;

/* [USER CAN EDIT] PID gain for controller 1 and 2 */
float kp = 0.5;
float ki = 0.0;
float kd = 6.0;

float kp2 = 0.2;
float ki2 = 0.0;
float kd2 = 12.0;

/* Sensor array: current */
double cur_sensors[NUM_SENSORS] = {0};

/* Sensor array: previous */
double prev_sensors[NUM_SENSORS] = {0};

/* Sensor array: frozen counter */
int frozen_counter[NUM_SENSORS] = {0};

/* Keeps track which sensor(s) is frozen */
int is_frozen[NUM_SENSORS] = {0};

/* Sensor mean value array */
float mean_sensors[NUM_SENSORS/2] = {0};

/* Multiplexer select pins */
int s0 = 5;
int s1 = 6;
int s2 = 7;

/* Multiplexer select values */
int r0 = 0;
int r1 = 0;
int r2 = 0;

/* Controller parameters */
unsigned long prev_time = 0;
unsigned long cur_time = 0;
float dtime = 0.0;
float input = 0.0;
float output = 0.0;
float setpoint = 0.0;
float error = 0.0;
float derror = 0.0;
float error_sum = 0.0;
float prev_error = 0.0;

unsigned long prev_time2 = 0;
unsigned long cur_time2 = 0;
float dtime2 = 0.0;
float input2 = 0.0;
float output2 = 0.0;
float setpoint2 = 0.0;
float error2 = 0.0;
float derror2 = 0.0;
float error_sum2 = 0.0;
float prev_error2 = 0.0;

/* Servo */
Servo servo1;
Servo servo2;

float cur_servo1 = 88;
float cur_servo2 = 73;

void setup() {
	Serial.begin(9600);

	/* Set select pins of the multiplexer as output type */
	pinMode(s0, OUTPUT);
	pinMode(s1, OUTPUT);
	pinMode(s2, OUTPUT);

	/* Initialize current sensor values */
	set_current_sensors();

	/* Initialize servo */
	servo1.attach(9);
	servo2.attach(10);

	servo1.write(cur_servo1); // 20 - 90 - 162 
	servo2.write(cur_servo2); // 10 - 160
}

/* Set current sensor array */
void set_current_sensors(void) {
	for(int i = 0; i < NUM_SENSORS; i++) {
		r0 = bitRead(i, 0);
		r1 = bitRead(i, 1); 
		r2 = bitRead(i, 2); 

		digitalWrite(s0, r0);
		digitalWrite(s1, r1);
		digitalWrite(s2, r2);

		cur_sensors[i] = analogRead(A0);
	}
}

/* Set previous sensor array */
void set_previous_sensors(void) {
	for(int i = 0; i < NUM_SENSORS; i++) {
		prev_sensors[i] = cur_sensors[i];
	}
}

/* Detect and correct for random noise */
void edc_noise(void) {
	int delta1 = 0;
	int delta2 = 0;

	for(int i = 0; i < NUM_SENSORS/2; i++) {
		delta1 = abs(cur_sensors[i*2] - prev_sensors[i*2]);
		delta2 = abs(cur_sensors[i*2+1] - prev_sensors[i*2+1]);

		if((delta1 > threshold1) != (delta2 > threshold1)) {
			if(delta1 > threshold1) {
				cur_sensors[i*2] = prev_sensors[i*2];
			} else  {
				cur_sensors[i*2+1] = prev_sensors[i*2+1];
			}
		}
	}
}

/* Detect and correct for each sensor if the value is frozen */
void edc_frozen(void) {
	int delta1 = 0;
	int delta2 = 0;

	for(int i = 0; i < NUM_SENSORS/2; i++) {
		delta1 = abs(cur_sensors[i*2] - prev_sensors[i*2]);
		delta2 = abs(cur_sensors[i*2+1] - prev_sensors[i*2+1]);

		if(delta1 == 0) {
			if(!is_frozen[i*2]) {
				frozen_counter[i*2] += 1;
			}
		} else {
			if(!is_frozen[i*2]) {
				frozen_counter[i*2] = 0;
			}
		}

		if(delta2 == 0) {
			if(!is_frozen[i*2+1]) {
				frozen_counter[i*2 + 1] += 1;
			}
		} else {
			if(!is_frozen[i*2+1]) {
				frozen_counter[i*2 + 1] = 0;
			}
		}

		if(frozen_counter[i*2] > threshold2) {
			cur_sensors[i*2] = cur_sensors[i*2+1];
			is_frozen[i*2] = 1;
		}

		if(frozen_counter[i*2+1] > threshold2) {
			cur_sensors[i*2+1] = cur_sensors[i*2];
			is_frozen[i*2+1] = 1;
		}
	}
}

/* Error detection and correction for the sensors */
void edc(void) {
	edc_noise();
	edc_frozen();
}

/* Compute mean sensor values for each vertex */
void set_mean_sensors(void) {
	for(int i = 0; i < NUM_SENSORS/2; i++) {
		mean_sensors[i] = (cur_sensors[i*2] + cur_sensors[i*2+1])/2;
	}
}

float controller(void) {
	cur_time = millis();
	dtime = (float) (cur_time - prev_time);
	input = mean_sensors[2] - mean_sensors[0];
	error = setpoint - input;
	error_sum += (error * dtime);
	derror = (error - prev_error) / dtime;

	output = kp * error + ki * error_sum + kd * derror;

	prev_error = error;
	prev_time = cur_time;

	return output;
}

float controller_2(void) {
	cur_time2 = millis();
	dtime2 = (float) (cur_time2 - prev_time2);
	input2 = mean_sensors[1] - mean_sensors[3];
	error2 = setpoint2 - input2;
	error_sum2 += (error2 * dtime2);
	derror2 = (error2 - prev_error2) / dtime2;

	output2 = kp2* error2 + ki2  * error_sum2 + kd2 * derror2;

	prev_error2 = error2;
	prev_time2 = cur_time2;

	return output2;
}

void set_output(void) {
	float output = 0.0;
	float output2 = 0.0;
	output = controller();
	output2 = controller_2();

	cur_servo1 = cur_servo1 + output;
	cur_servo1 = constrain(cur_servo1, 20, 150);
	cur_servo2 = cur_servo2 + output2;
	cur_servo2 = constrain(cur_servo2, 20, 140);
	servo1.write(cur_servo1);
	servo2.write(cur_servo2);
}

void loop() {
	/* Transmit header */
	Serial.println(999);
	
	set_previous_sensors();
	set_current_sensors();
	edc();
	set_mean_sensors();
	set_output();
	

	/* Servo1: 13 - 160
	 * Servo2: 15 - 150
	 */
	Serial.println(error);
	Serial.println(error2);

	delay(100);
}

